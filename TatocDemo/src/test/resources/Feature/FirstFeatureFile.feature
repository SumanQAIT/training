Feature: Tatoc Homepage
This feature verifies the functionality on Tatoc Basic website.
 
Scenario: Check that main elements on Tatoc are working
Given I launch Chrome browser and Enter Tatoc URL
When click on /tatoc
And click on Basic Course
And click on Green box 
And click on Repaint box until the color of both boxes get matched,then click Proceed
And Drag and drop a button then proceed
And click Launch Popup window
And click generate token 
Then I verify that the page displays with End Page title

