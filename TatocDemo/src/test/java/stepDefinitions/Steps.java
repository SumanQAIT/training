package stepDefinitions;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps {

	
	WebDriver driver;
	String nodeUrl;
	
@Given("^I launch Chrome browser and Enter Tatoc URL$")
   public void launchBrower()
   {
	   System.setProperty("webdriver.chrome.driver", "C:\\Users\\sumankumawat\\Downloads\\chromedrivernew\\chromedriver.exe");
		driver = new ChromeDriver();
	   
	   
		
		driver.get("http://10.0.1.86/");
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		
		//Implicit Wait
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	   
   }
   
   public void seleniumGridExecution() throws MalformedURLException
   {
	   nodeUrl = "http://10.0.13.180:4444/wd/hub";
	   DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	   capabilities.setBrowserName("chrome");
	   capabilities.setPlatform(Platform.WINDOWS);
	   
	   driver = new RemoteWebDriver(new URL(nodeUrl), capabilities);
	   
	   driver.get("http://10.0.1.86/");
	   
	  
	   
   }
   
  @When("^click on /tatoc$")
   
   public void clickTatoc()
   {
	   //Concept of findElement
	   WebElement a =  driver.findElement(By.xpath("//a[@href = '/tatoc']"));
	   
	   //Explicit wait
	   WebDriverWait wait = new WebDriverWait(driver,15);
	   wait.until(ExpectedConditions.visibilityOf(a));
	   a.click();
	     
	   
   }
  
  @When("^click on Basic Course$")
   
   public void clickOnTacosBasic()
   {
	   driver.findElement(By.xpath("//a[@href = '/tatoc/basic']")).click();
   }
   public String getTitle()
   {
	   String title = driver.getTitle();
	   return title;
	   
   }
   
   @When("^click on Green box$")
   public void clickOnGreenBox()
   
   {
	   //Concept of FindElements (It will give total number of row in the box)
	   
	   List <WebElement> list =  driver.findElements(By.tagName("tr"));
	   int size = list.size();
	   System.out.println("The total number of rows are "+size);
	   
	   //By using className locator
	   
	   driver.findElement(By.className("greenbox")).click();
   }
   
   @When("^click on Repaint box until the color of both boxes get matched,then click Proceed$")
   public void clickOnOuterFrame() throws InterruptedException
   {
	   driver.switchTo().frame("main");
	   WebElement box1 = driver.findElement(By.xpath("//div[contains(text(),'Box 1')]"));
	   String box1Color = box1.getAttribute("class");
	   driver.switchTo().frame("child");
	   WebElement box2 = driver.findElement(By.xpath("//div[contains(text(),'Box 2')]"));
	   String box2Color = box2.getAttribute("class");
	   System.out.println("box1Color1"+box1Color);
	   System.out.println("box1Color2"+box2Color);
	   driver.switchTo().defaultContent();
	   driver.switchTo().frame("main");
	   WebElement repaintLink = driver.findElement(By.xpath("//a[contains(text(),'Repaint Box 2')]"));
	  
	   
	   while(!(box2Color.equalsIgnoreCase(box1Color)))
	   {
		   driver.switchTo().defaultContent();
		   driver.switchTo().frame("main");
		   repaintLink.click();
		   System.out.println("clicked on repaint"); 
		   driver.switchTo().frame("child");
		   Thread.sleep(1000);
		   //box2Color = box2.getAttribute("class");
		   box2Color = driver.findElement(By.xpath("//div[contains(text(),'Box 2')]")).getAttribute("class");
	   }   
	            driver.switchTo().defaultContent();
			   driver.switchTo().frame("main");
			   driver.findElement(By.xpath("//a[contains(text(),'Proceed')]")).click();
			     
	   }
   @When("^Drag and drop a button then proceed$")
   public void dragAndDropMethod() throws InterruptedException
   {
	   
	  
	   WebElement sourceElement = driver.findElement(By.xpath("//div[@id = 'dragbox']"));
	   WebElement destElement = driver.findElement(By.xpath("//div[@id = 'dropbox']"));
	   
	   Actions action = new Actions(driver);
	   Thread.sleep(2000);
	   action.dragAndDrop(sourceElement, destElement).build().perform();
	 
	   
	   driver.findElement(By.xpath("//a[contains(text(),'Proceed')]")).click();
	   
   }
   
   @When("^click Launch Popup window$")
   public void newWindowHandle()
   {
	   String parentWindow = driver.getWindowHandle();
	   driver.findElement(By.xpath("//a[contains(text(),'Launch Popup Window')]")).click();
	   ArrayList<String> tabs = new ArrayList <String>(driver.getWindowHandles());
	    System.out.println(tabs.size());
	    driver.switchTo().window(tabs.get(1)); 
	    
	    driver.findElement(By.id("name")).sendKeys("suman");
	    driver.findElement(By.id("submit")).click();
	    driver.switchTo().window(parentWindow);
	    
	    driver.findElement(By.xpath("//a[contains(text(),'Proceed')]")).click();
	   
   }
   @When("^click generate token$")
   
   public void cookieHandling()
   {
	   driver.findElement(By.xpath("//a[contains(text(),'Generate Token')]")).click();
	   String tokenValue = driver.findElement(By.id("token")).getText().toString();
	   
	   String newTokenValue=  tokenValue.replaceAll("Token: ", "");
	   System.out.println(newTokenValue);
	   Cookie name = new Cookie("Token", newTokenValue);
	   driver.manage().addCookie(name);
	   
	   driver.findElement(By.xpath("//a[contains(text(),'Proceed')]")).click();
	   
	   
   }
   
   @Then("^I verify that the page displays with End Page title$")
   public void verifyEndPage()
   {
	   String actualTitle = driver.getTitle();
	   String expectedTitle = "End - T.A.T.O.C";
	   
	   if(actualTitle.equals(expectedTitle))
	   {
		   System.out.println("Successful");
	   }
   }
   
   
}
