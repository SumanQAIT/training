package com.qait.automation.TatocDemo;

import java.net.MalformedURLException;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Unit test for simple App.
 */
public class AppTest
{
	App a1 = new App();
	
	@Test
   public void openBrowser() throws MalformedURLException
	{
		System.out.println("Before test");
		//a1.seleniumGridExecution();
		a1.launchBrower();
		
			
	}
	
	@Test (dependsOnMethods = "openBrowser")
	public void clickOnTatoc()
	{
		String actual = a1.getTitle();
		String expected = "TAP Utility Server";
		Assert.assertEquals(actual, expected, "The Title doesnt match");
		a1.clickTatoc();
			
		
	}
	
	@Test (dependsOnMethods = "clickOnTatoc")
	public void clickOnTacosBasic()
	{
		String actual = a1.getTitle();
		String expected = "Welcome - T.A.T.O.C";
		Assert.assertEquals(actual, expected, "The Title doesnt match");
		a1.clickOnTacosBasic();	
		
	}
	
	@Test (dependsOnMethods = "clickOnTacosBasic")
	public void clickOnGreenBox()
	{
		String actual = a1.getTitle();
		String expected = "Grid Gate - Basic Course - T.A.T.O.C";
		Assert.assertEquals(actual, expected, "The Title doesnt match");
		a1.clickOnGreenBox();
		
		
	}
	
	@Test (dependsOnMethods = "clickOnGreenBox")
	 public void clickOnFrame()
	 {
		String actual = a1.getTitle();
		String expected = "Frame Dungeon - Basic Course - T.A.T.O.C";
		Assert.assertEquals(actual, expected, "The Title doesnt match");
		try {
			a1.clickOnOuterFrame();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	 }	 
	
	@Test (dependsOnMethods = "clickOnFrame" )
	
	public void dragAndDropActions() throws InterruptedException
	{
		String actual = a1.getTitle();
		String expected = "Drag - Basic Course - T.A.T.O.C";
		Assert.assertEquals(actual, expected, "The Title doesnt match");
		a1.dragAndDropMethod();
		
	}
	
	@Test (dependsOnMethods = "dragAndDropActions")
	
	public void getNewWindowHandle()
	{
		String actual = a1.getTitle();
		String expected = "Windows - Basic Course - T.A.T.O.C";
		Assert.assertEquals(actual, expected, "The Title doesnt match");
		a1.newWindowHandle();
		
	}
	
	@Test (dependsOnMethods = "getNewWindowHandle")
	public void cookieHandlingMethod()
	{
		String actual = a1.getTitle();
		String expected = "Cookie Handling - Basic Course - T.A.T.O.C";
		Assert.assertEquals(actual, expected, "The Title doesnt match");
		a1.cookieHandling();
		
	}
}
	
	





